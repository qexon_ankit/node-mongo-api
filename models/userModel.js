const db = require('../database');
const mongoSchema = db.mongoSchema;

const userSchema = {
    'email' : String,
    'password' : String
}

module.exports = db.model('users',userSchema);