const express = require('express');
const router = express.Router();
const userController = require('./controllers/userController')

router.get('/',userController.userInformation);
router.get('/save',userController.save);

module.exports = router;
